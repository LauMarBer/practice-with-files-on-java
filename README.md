What the practice consists of:

It is about managing a list of products that can belong to two categories: sales and purchases. For each product, its code, name, type and price are stored. 

The application consists of a menu that allows:
1. Create an XML file with data of different products entered by keyboard. 
2. Display all products, separated by the categories sold and purchased.
3. Show only those products whose price is higher than the one read by keyboard. 
4. Show only those products that correspond to a type entered on the keyboard.
5. Show the products of a type entered by keyboard and whose price is higher than the average. 
6. Show all products that belong to the sales category. 
7. Save in a CSV file with the structure "code:name:price" only the items that have a price higher than 2.99 and are of type A.
8. Create a shortcut file with the structure "sales/purchases, code, name, price".
9. From the shortcut file, create an xml file with more reduced information than the initial file.
10. Ask by keyboard to enter a product type and delete all products matching that type from the main xml file. 


Notice that the main xml file already exits with the name "datos.xml" and "datos-copiaseguridad.xml", the latest one as a save copy of the first one. However, it is possible to delete the mentioned files and create a new one with the first option of the application menu. This files exits just as an example for the rest of the options, so you don't have to go through option one to use the other options. The CSV and RandomAccess files are deleted at first but there is no problem at all if they already exits. 
