package Res;

import java.util.ArrayList;
import java.util.Arrays;

public class Constants {
	
	//File paths I work with:
	public static final String FILE_PATH ="datos.xml";
	public static final String XMLREDUCFILE_PATH ="datosReducido.xml";
	public static final String CSVFILE_PATH ="datos.csv";
	public static final String RANDOMFILE_PATH = "datos.dat";
	public static final String FORMATFILE_PATH ="f.xslt";
	
	//Main menu:
	public static final ArrayList<String> ENUNCIADO = new ArrayList<>(
			Arrays.asList("*********** COMPRAS Y VENTAS *************",
							"Por favor, introduce una de las siguiente opciones:",
							"1. Insertar datos por teclado en el archivo XML",
							"2. Mostrar los datos del fichero XML",
							"3. Mostrar los productos cuyo precio sea superior a un precio dado", 
							"4. Mostrar los productos de un tipo le�do por teclado",
							"5. Mostrar los productos de un tipo cuyo precio sea superior a la media",
							"6. Mostrar todos los productos de los que se hayan realizado ventas",
							"7. Guardar en un archivo CSV todos los productos de tipo 'A' con un precio superior a 2.99",
							"8. Crear un fichero de acceso directo con la estructura ventas/compras, c�digo, nombre, precio",
							"9. A partir del fichero de acceso directo crear un xml",
							"10. Eliminar todos los productos que sean del tipo introducido por el usuario",
							"0. Salir"));
	
	//Case 1 submenu to insert new product data: 
	public static final ArrayList<String> INSERT_PRODUCT_DATA = new ArrayList<>(
			Arrays.asList("***************NUEVO PRODUCTO*****************",
							"Por favor, rellena los siguientes campos:",
							"C�digo (un n�mero entero): ",
							"Tipo (un �nico caracter): ",
							"Nombre: ",
							"Descripci�n: ",
							"Precio (n�mero decimal): "));
	
	//Case 1 submenu to choose at which label the user wants to insert the product:
	public static final ArrayList<String> PLACE_PRODUCT_INSERT = new ArrayList<>(
			Arrays.asList("Indica d�nde deseas almacenarlo: ",
							"1. En ventas ",
							"2. En compras",
							"3. En ambos"));
	
	//The all table's header:
	public static final String[] TABLE_HEADER = new String[] {
			"C�DIGO",
			"TIPO",
			"NOMBRE",
			"DESCRIPCI�N",
			"PRECIO"
	};
	
	//Titles of the tables:
	public static final String TABLE_SALES_TITLE = "PRODUCTOS VENDIDOS";
	public static final String TABLE_PURCHASE_TITLE = "PRODUCTOS COMPRADOS";
	public static final String TABLE_ALLPRODUCTS_TITLE = "TODOS LOS PRODUCTOS";
	
	//Requests to the user:
	public static final String PRICE_REQUEST= "Introduce un precio: ";
	public static final String TYPE_REQUEST= "Introduce un tipo: ";
	public static final String CONTINUE_CONFIRM_REQUEST = "�Est� seguro? Pulse 1 para confirmar y 0 para cancelar";
	public static final String KEEPING_CONFIRM_REQUEST = "Para introducir m�s productos, pulse 1. Para salir, pulse 0";
	
	//Error messages:
	public static final String CREATEFILE_ERROR_MESSAGE = "No se ha podido crear el archivo correctamente, por lo que no se puede acceder a �l";
	public static final String EXCEPTION_ERROR_MESSAGE = "Ha ocurrido un problema durante la manipulaci�n del archivo. Por favor, reinicie la aplicaci�n e int�ntelo de nuevo";
	public static final String RENAMING_DELETING_ERROR = "Los nuevos ficheros no se han podido gestionar correctamente";
	public static final String OPTION_ERROR_MESSAGE = "El n�mero introducido no corresponde a ninguna opci�n";
	public static final String FORMAT_ERROR_MESSAGE = "El formato introducido no es correcto. Por favor, introduzca el dato en el formato solicitado";
	public static final String NOTFOUND_ERROR_MESSAGE = "El fichero al que desea acceder no existe en la ruta especificada";
	public static final String DOCUMENT_MANAGEMENT_ERROR = "Se ha producido un error al manejar internamente los documentos";
	
	//Information messages:
	public static final String BYE_MESSAGE = "�Hasta pronto!";
	public static final String SUCCESS_PROCESS = "�Operaci�n realizada correctamente!";
	public static final String CANCELED_MESSAGE = "Operaci�n cancelada";
	public static final String EMPTY_SALES = "La etiqueta \"ventas\" no contiene ning�n producto";
	public static final String EMPTY_PURCHASE = "La etiqueta \"compras\" no contiene ning�n producto";
	public static final String EMPTY_PRODUCTS = "No existen productos que mostrar";
	

}
