package controlador;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.lang.module.ModuleDescriptor.Builder;
import java.util.ArrayList;

import javax.sound.sampled.spi.FormatConversionProvider;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import Res.Constants;
import modelo.Product;
import vista.ProductForm;

public class XMLManagement {
	private Document newDoc, existingDoc;
	private File xml_file, csv_file, random_file;

	public XMLManagement(String file_name) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			//Initializing the file objects.
			this.xml_file = new File(file_name);
			this.csv_file = new File(Constants.CSVFILE_PATH);
			this.random_file = new File(Constants.RANDOMFILE_PATH);
			
			this.newDoc = builder.newDocument();
			 
			//I check if the file exists or not, before trying to associate it with the existingDoc object. 
			if(this.xml_file.exists()) {
				this.existingDoc = builder.parse(this.xml_file);
			}
		}catch(Exception e) {
			System.err.println(Constants.DOCUMENT_MANAGEMENT_ERROR);
		}
	}
	
	public void createNewDocument() {
		//Document root element
		Element root = this.newDoc.createElement("datos");
		this.newDoc.appendChild(root);
		
		//Sales and purchase elements
		Element sales =  this.newDoc.createElement("ventas");
		Element purchase =  this.newDoc.createElement("compras");
		root.appendChild(sales);
		root.appendChild(purchase);
		
		//Products elements included at sales and purchase. 
		Element productssales =  this.newDoc.createElement("productos");
		Element productspurchase =  this.newDoc.createElement("productos");
		sales.appendChild(productssales);
		purchase.appendChild(productspurchase);
	}
	
	public void insertProduct(Product p, int addOption) {
		
		//It may happen that the xml_file hadn't been created yet. So, I need to ensure that the existingDoc is not null.
		if(!xml_file.exists()) {
			createNewDocument();
			createXML(true);
		}
	
		//Product Element:
		Element product = this.existingDoc.createElement("producto");
			//Product's attributes:
		product.setAttribute("codigo", String.valueOf(p.getCode()));
		product.setAttribute("tipo", String.valueOf(p.getType()));
		
			//Product's child labels with their values:
		Element name = this.existingDoc.createElement("nombre");
		name.setTextContent(p.getName());
		product.appendChild(name);
		Element description = this.existingDoc.createElement("descripcion");
		description.setTextContent(p.getDescription());
		product.appendChild(description);
		Element price = this.existingDoc.createElement("precio");
		price.setTextContent(String.valueOf(p.getPrice()));
		product.appendChild(price);
		
		//The previous labels containing product elements. 
		Element sales = (Element)this.existingDoc.getElementsByTagName("ventas").item(0);
		Element productssales = (Element)sales.getElementsByTagName("productos").item(0);
		
		Element purchase = (Element)this.existingDoc.getElementsByTagName("compras").item(0);
		Element productspurchase = (Element)purchase.getElementsByTagName("productos").item(0);

		switch (addOption) {
			case 1:
				productssales.appendChild(product);
				break;
			case 3:
				Element product_2 = (Element) product.cloneNode(true);
				productssales.appendChild(product_2);
			case 2:
				productspurchase.appendChild(product);
		}
	}
	
	public void insertProductReduced(Product p, String pclassification) {
		
		//It may happen that the xml_file hadn't been created yet. So, I need to ensure that the existingDoc is not null.
		if(!xml_file.exists()) {
			createNewDocument();
			createXML(true);
		}
	
		//Product Element:
		Element product = this.existingDoc.createElement("producto");
			//Product's attributes:
		product.setAttribute("codigo", String.valueOf(p.getCode()));
		
			//Product's child labels with their values:
		Element name = this.existingDoc.createElement("nombre");
		name.setTextContent(p.getName());
		product.appendChild(name);
		Element price = this.existingDoc.createElement("precio");
		price.setTextContent(String.valueOf(p.getPrice()));
		product.appendChild(price);
		
		//The previous labels containing product elements. 
		Element sales = (Element)this.existingDoc.getElementsByTagName("ventas").item(0);
		Element productssales = (Element)sales.getElementsByTagName("productos").item(0);
		
		Element purchase = (Element)this.existingDoc.getElementsByTagName("compras").item(0);
		Element productspurchase = (Element)purchase.getElementsByTagName("productos").item(0);
		
		if(pclassification.equalsIgnoreCase("ventas")) {
			productssales.appendChild(product);
		}else if(pclassification.equalsIgnoreCase("compras")) {
			productspurchase.appendChild(product);
		}
		
	}
	
	
	public void createXML(boolean overwritee) {
		try {
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer(new StreamSource(new File(Constants.FORMATFILE_PATH)));
			
			Document doc;
			if(overwritee) {
				xml_file.createNewFile();
				doc = this.newDoc;
			}else {
				doc = this.existingDoc;
			}
			Source source = new DOMSource(doc);
			Result result = new StreamResult(xml_file);
			
			transformer.setOutputProperty(OutputKeys.INDENT,"yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount","4");
			
			transformer.transform(source, result);
			
			if(!overwritee) {
				System.out.println(Constants.SUCCESS_PROCESS);
			}
			
			
		}catch (TransformerException e) {
			System.err.println(Constants.EXCEPTION_ERROR_MESSAGE);
		}catch(IOException e) {
			System.err.println(Constants.EXCEPTION_ERROR_MESSAGE);
		}catch (Exception e) {
			System.err.println(Constants.EXCEPTION_ERROR_MESSAGE);
		}
	}
	
	
	public NodeList readSold_Products() {
		NodeList childnodes = null;
		
		if(xml_file.exists()) {
			Element sales = (Element)existingDoc.getElementsByTagName("ventas").item(0);
			Element productssales = (Element)sales.getElementsByTagName("productos").item(0);
			if(productssales.hasChildNodes()) {
				childnodes = productssales.getChildNodes();
				
			}else {
				System.out.println(Constants.EMPTY_SALES);
			}
		}else {
			System.err.println(Constants.NOTFOUND_ERROR_MESSAGE);
		}
		
		return childnodes;
		
	}
	
	public NodeList readBought_Products() {
		NodeList childnodes = null;
		
		if(xml_file.exists()) {
			Element purchase = (Element)existingDoc.getElementsByTagName("compras").item(0);
			Element productspurchase = (Element)purchase.getElementsByTagName("productos").item(0);
			if(productspurchase.hasChildNodes()) {
				childnodes = productspurchase.getChildNodes();
				
			}else {
				System.out.println(Constants.EMPTY_PURCHASE);
			}
		}else {
			System.err.println(Constants.NOTFOUND_ERROR_MESSAGE);
		}
		
		return childnodes;
		
	}
	
	public Product readProduct(Node product) {
		Product p = new Product();
		p.setCode(Integer.parseInt(((Element) product).getAttribute("codigo")));
		p.setType(((Element) product).getAttribute("tipo").charAt(0));
		p.setName(((Element) product).getElementsByTagName("nombre").item(0).getTextContent());
		p.setDescription(((Element) product).getElementsByTagName("descripcion").item(0).getTextContent());
		p.setPrice(Float.parseFloat(((Element) product).getElementsByTagName("precio").item(0).getTextContent()));
	
		return p;
	}
	
	//Case 3 and 4 at the Main Class:
	public <T> void compareAndPrintTable(T data) {
		ProductForm form = new ProductForm();
		
		//First of all, I have to print the header of the table.
		form.printHeaderTable(Constants.TABLE_ALLPRODUCTS_TITLE);
	
		//Now, I can add products to the table body:
		if(xml_file.exists()) {
			NodeList all_products = existingDoc.getElementsByTagName("productos");
			for (int i = 0; i < all_products.getLength(); i++) {
				Node products = all_products.item(i);
				NodeList childnodes = products.getChildNodes();
				for (int j = 0; j < childnodes.getLength(); j++) {
					Node product = childnodes.item(j);
					if(product instanceof Element) {
						if(data instanceof Character && ((Element) product).getAttribute("tipo").charAt(0)==(char)data) {
							Product p = readProduct(product);
							form.printProductTable(p);
						}else if(data instanceof Float && Float.parseFloat(((Element) product).getElementsByTagName("precio").item(0).getTextContent()) > (float)data){
							Product p = readProduct(product);
							form.printProductTable(p);
						}
					}
				}
			}
			//Finally, I print the foot of the table:
			form.printFootTable();
		}else {
			System.err.println(Constants.NOTFOUND_ERROR_MESSAGE);
		}
			
	}
	
	//Case 5 at the Main Class:
	public float getPriceAverage() {
		float average = 0;
		int products_number = 0;
		
		if(xml_file.exists()) {
			NodeList all_products = existingDoc.getElementsByTagName("productos");
			for (int i = 0; i < all_products.getLength(); i++) {
				Node products = all_products.item(i);
				NodeList childnodes = products.getChildNodes();
				for (int j = 0; j < childnodes.getLength(); j++) {
					Node product = childnodes.item(j);
					if(product instanceof Element) {
						Product p = readProduct(product);
						average+=p.getPrice();
						products_number++;
					}
				}
			}
			average /=products_number;
		}
		return average;
	}
	
	//Case 5 at the Main Class:
	public void compareAndPrintTable(float price, char type) {
		ProductForm form = new ProductForm();
		
		//First of all, I have to print the header of the table.
		form.printHeaderTable(Constants.TABLE_ALLPRODUCTS_TITLE);
		
		//Now, I can add products to the table body:
		if(xml_file.exists()) {
			NodeList all_products = existingDoc.getElementsByTagName("productos");
			for (int i = 0; i < all_products.getLength(); i++) {
				Node products = all_products.item(i);
				NodeList childnodes = products.getChildNodes();
				for (int j = 0; j < childnodes.getLength(); j++) {
					Node product = childnodes.item(j);
					if(product instanceof Element) {
						if(((Element) product).getAttribute("tipo").charAt(0)==type && Float.parseFloat(((Element) product).getElementsByTagName("precio").item(0).getTextContent()) > price){
							Product p = readProduct(product);
							
							form.printProductTable(p);
						}
					}
				}
			}
			//Finally, I print the foot of the table:
			form.printFootTable();
		}else {
			System.err.println(Constants.NOTFOUND_ERROR_MESSAGE);
		}
			
	}
	
	//Case 2 and 6 at the Main Class:
	public void printProducts(NodeList childnodes, String title) {
		ProductForm form = new ProductForm();
		
		if(childnodes!=null) {
			//Header of the table:
			form.printHeaderTable(title);
			
			//Body of the table:
			for (int i = 0; i < childnodes.getLength(); i++) {
				Node product = childnodes.item(i);
				if(product instanceof Element) {
					Product p = readProduct(product);
					form.printProductTable(p);
				}
			}
			
			//Footer of the table:
			form.printFootTable();
		}
	}
	
	//Case 7 at the Main Class:
	public void compareAndWriteCSV(float price, char type) {
		//Before the software starts to insert more products on the csv, I must ensure that the file is empty. 
		//This way, I can add items to the csv file at its end with the method writeCSV and the file will not contain any previous data. 
		if(csv_file.exists()) {
			csv_file.delete();
		}
		
		if(xml_file.exists()) {
			NodeList all_products = existingDoc.getElementsByTagName("productos");
			for (int i = 0; i < all_products.getLength(); i++) {
				Node products = all_products.item(i);
				NodeList childnodes = products.getChildNodes();
				for (int j = 0; j < childnodes.getLength(); j++) {
					Node product = childnodes.item(j);
					if(product instanceof Element) {
						if(((Element) product).getAttribute("tipo").charAt(0)==type && Float.parseFloat(((Element) product).getElementsByTagName("precio").item(0).getTextContent()) > price){
							Product p = readProduct(product);
							
							writeCSV(p);
						}
					}
				}
			}
			System.out.println(Constants.SUCCESS_PROCESS);
		}else {
			System.err.println(Constants.NOTFOUND_ERROR_MESSAGE);
		}
			
			
	}
	
	

	public void writeCSV(Product product) {
		try {
			csv_file.createNewFile();
			if(csv_file.exists()) {
				//If the file exists, I can write new products at its end.
				PrintWriter pw = new PrintWriter(new FileWriter(csv_file, true));
				pw.print(product.getCode());
				pw.print(":");
				pw.print(product.getName());
				pw.print(":");
				pw.println(product.getPrice());
				
				pw.close();
				
			}else {
				System.err.println(Constants.NOTFOUND_ERROR_MESSAGE);
			}
		}catch(IOException e) {
			System.err.println(Constants.EXCEPTION_ERROR_MESSAGE);
		}
	}
	
	//Case 8 at the Main Class:
	public void readAndWriteRand() {
		//Before the software starts to insert more products on the file, I must ensure that it's empty. 
		//This way, I can add items at its end with the method writeRandFile and the file will never contain any previous data. 
		if(random_file.exists()) {
			random_file.delete();
		}
		
		//Now, I read the products at the xml_file:
		if(xml_file.exists()) {
			//Sales products:
			NodeList childnodes_salesp = readSold_Products();
			for (int i = 0; i < childnodes_salesp.getLength(); i++) {
				Node product = childnodes_salesp.item(i);
					if(product instanceof Element) {
						Product p = readProduct(product);
						writeRandFile("ventas", p);
					}
				
			}
			
			//Now, bought products:
			NodeList childnodes_purchasep = readBought_Products();
			for (int i = 0; i < childnodes_purchasep.getLength(); i++) {
				Node product = childnodes_purchasep.item(i);
					if(product instanceof Element) {
						Product p = readProduct(product);
						writeRandFile("compras", p);
					}
				
			}
			System.out.println(Constants.SUCCESS_PROCESS);
		}else {
			System.err.println(Constants.NOTFOUND_ERROR_MESSAGE);
		}
	}
	
	//Adds elements to the randomAccessFile:
	public void writeRandFile(String p_classification, Product p) {
		RandomAccessFile raf = null;
		
		try {
			random_file.createNewFile();
			if(random_file.exists()) {
				raf = new RandomAccessFile(random_file, "rw");
				
				//Before I start to write, I need to locate the pointer at the end of the file:
				raf.seek(random_file.length());
				
				//Writting data:
				//Sales or purchase:
				StringBuffer strBuff = new StringBuffer(p_classification);
				strBuff.setLength(7);
				raf.writeChars(strBuff.toString());
				//Code
				raf.writeInt(p.getCode());
				//Name:
				StringBuffer strBuffname = new StringBuffer(p.getName());
				strBuffname.setLength(20);
				raf.writeChars(strBuffname.toString());
				//Price:
				raf.writeFloat(p.getPrice());
				
				raf.close();
			}
		}catch (IOException e) {
			System.err.println(Constants.EXCEPTION_ERROR_MESSAGE);
		}catch(Exception e) {
			System.err.println(Constants.EXCEPTION_ERROR_MESSAGE);
		}
	}
	
	//Reads the randomAccessFile and writes a xml file:
	public void readRandWriteXML() {
		RandomAccessFile raf = null;
		
		try {
			if(random_file.exists()) {
				raf = new RandomAccessFile(random_file, "r");
				
				while(raf.getFilePointer()!=raf.length()) {
					Product p = new Product();
					
					//Reads sales or purchase:
					char[] classification = new char[7];
					for (int i = 0; i <classification.length ; i++) {
						classification[i]=raf.readChar();
					}
					String pclassification = new String(classification);
					pclassification=pclassification.trim();
					//Reads the code: 
					p.setCode(raf.readInt());
					//Reads the name:
					char[] name = new char[20];
					for (int i = 0; i <name.length ; i++) {
						name[i]=raf.readChar();
					}
					String namee = new String(name);
					namee = namee.trim();
					p.setName(namee);
					//Reads the price:
					p.setPrice(raf.readFloat());
					
					insertProductReduced(p, pclassification);
				}
				
				raf.close();
			}else {
				System.err.println(Constants.NOTFOUND_ERROR_MESSAGE);
			}
		}catch (Exception e) {
			System.err.println(Constants.EXCEPTION_ERROR_MESSAGE);
		}
	}
	
	//Remove all the products, whose type is equals to the introduced by the user. 
	public void removeProducts(char type) {
		
		if(xml_file.exists()) {
			NodeList all_products = existingDoc.getElementsByTagName("productos");
			for (int i = 0; i < all_products.getLength(); i++) {
				Node products = all_products.item(i);
				NodeList childnodes = products.getChildNodes();
				for (int j = 0; j < childnodes.getLength(); j++) {
					Node product = childnodes.item(j);
					if(product instanceof Element) {
						if(((Element) product).getAttribute("tipo").charAt(0)==type) {
							product.getParentNode().removeChild(product);
						}
					}
				}
			}
		}else {
			System.err.println(Constants.NOTFOUND_ERROR_MESSAGE);
		}
	}
	
}
