package controlador;


import Res.Constants;
import vista.ProductForm;

public class Main {

	public static void main(String[] args) {
		ProductForm form = new ProductForm();
		
		int option;
		do {
			form.printMenu();
			option = form.getNumberInt();
			
			//Because the user can modify the xml file, I have to instantiate this class at every loop iteration so I can access to the latest file version. 
			XMLManagement xml_management = new XMLManagement(Constants.FILE_PATH);
			
			switch(option) {
				case 1:
					//Creates the basic structure of the xml file overwriting the previous content. 
					xml_management.createNewDocument();
					xml_management.createXML(true);
					xml_management = new XMLManagement(Constants.FILE_PATH);
					
					//Now, users can add products to the basic structure just created. 
					do {
						xml_management.insertProduct(form.insertProductData(), form.placeInsertProduct());
					}while(form.keepInsertChoice());
					
					xml_management.createXML(false);
					break;
				case 2:
					xml_management.printProducts(xml_management.readSold_Products(), Constants.TABLE_SALES_TITLE);
					xml_management.printProducts(xml_management.readBought_Products(), Constants.TABLE_PURCHASE_TITLE);
					break;
				case 3:
					xml_management.compareAndPrintTable(form.getPrice());
					break;
				case 4:
					xml_management.compareAndPrintTable(form.getType());
					break;
				case 5:
					xml_management.compareAndPrintTable(xml_management.getPriceAverage(), form.getType());
					break;
				case 6:
					xml_management.printProducts(xml_management.readSold_Products(), Constants.TABLE_SALES_TITLE);
					break;
				case 7:
					xml_management.compareAndWriteCSV(2.99f, 'A');
					break;
				case 8:
					xml_management.readAndWriteRand();
					break;
				case 9:
					xml_management = new XMLManagement(Constants.XMLREDUCFILE_PATH);
					xml_management.createNewDocument();
					xml_management.createXML(true);
					xml_management = new XMLManagement(Constants.XMLREDUCFILE_PATH);
					
					xml_management.readRandWriteXML();
					xml_management.createXML(false);
					break;
				case 10:
					xml_management.removeProducts(form.getType());
					xml_management.createXML(false);
					break;
				case 0:
					System.out.println(Constants.BYE_MESSAGE);
					break;
				default:
					System.out.println(Constants.OPTION_ERROR_MESSAGE);
			}
			
		}while(option!=0);

	}

}
