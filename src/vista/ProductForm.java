package vista;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import Res.Constants;
import modelo.Product;

public class ProductForm {
Scanner teclado = new Scanner(System.in);
	
	//Prints the main menu:
	public void printMenu() {
		for (String enunciado : Constants.ENUNCIADO) {
			System.out.println(enunciado);
		}
	}
	
	//Allows to insert a number, checking that it's actually a number. While it's not, the system keep asking for another data input. 
	public int getNumberInt() {
		int numero=0;
		boolean correcto=false;
		
		do {
			try {
				numero=teclado.nextInt();
				teclado.nextLine();
				correcto=true;
			}catch(InputMismatchException e) {
				teclado.nextLine();
				System.err.println(Constants.FORMAT_ERROR_MESSAGE);
			}
		}while(!correcto);
		
		
		return numero;
		
	}
	
	//Allows to insert a float, checking that it's actually a float type. While it's not, the system keep asking for another data input.
	public float getNumberFloat() {
		float numero=0;
		boolean correcto=false;
		
		do {
			try {
				numero=teclado.nextFloat();
				teclado.nextLine();
				correcto= true;
			}catch(InputMismatchException e) {
				teclado.nextLine();
				System.err.println(Constants.FORMAT_ERROR_MESSAGE);
			}
		}while(!correcto);
		
		return numero;
		
	}

	//Allows to insert a String. 
	public String getText() {
		return teclado.nextLine();
			
	}
	
	//Asks if the user is sure about the operation he is about to do. 
	public boolean ConfirmOperation() {
		int respuesta;
		boolean confirm = false;
		
		do {
			System.out.println(Constants.CONTINUE_CONFIRM_REQUEST);
			respuesta = getNumberInt();
			
			switch(respuesta) {
				case 1:
					confirm = true;
					break;
				case 0:
					break;
				default:
					System.err.println(Constants.OPTION_ERROR_MESSAGE);
			}
		}while(respuesta!=1 && respuesta!=0);
		
		return confirm;
	}
	
	//Asks if the user wants to continue in that option or not. 
	public boolean keepInsertChoice() {
		int respuesta;
		boolean confirm = false;
		
		do {
			System.out.println(Constants.KEEPING_CONFIRM_REQUEST);
			respuesta = getNumberInt();
			
			switch(respuesta) {
				case 1:
					confirm = true;
					break;
				case 0:
					break;
				default:
					System.err.println(Constants.OPTION_ERROR_MESSAGE);
			}
		}while(respuesta!=1 && respuesta!=0);
		
		return confirm;
	}
	
	//Asks for product data:
	public Product insertProductData() {
		Product product = new Product();
		
		for (int i = 0; i < 2; i++) {
			System.out.println(Constants.INSERT_PRODUCT_DATA.get(i));
		}
		System.out.print(Constants.INSERT_PRODUCT_DATA.get(2));
		product.setCode(getNumberInt());
		
		System.out.print(Constants.INSERT_PRODUCT_DATA.get(3));
		product.setType(getText().charAt(0));
		
		System.out.print(Constants.INSERT_PRODUCT_DATA.get(4));
		product.setName(getText());
		
		System.out.print(Constants.INSERT_PRODUCT_DATA.get(5));
		product.setDescription(getText());
		
		System.out.print(Constants.INSERT_PRODUCT_DATA.get(6));
		product.setPrice(getNumberFloat());
		
		return product;
	}
	
	//Asks for the place the user wants to insert the product (sales or purchase).
	public int placeInsertProduct() {
		
		int option;
		do {
			for (String str : Constants.PLACE_PRODUCT_INSERT) {
				System.out.println(str);
			}
			option = getNumberInt();
			
			if(option<1 || option>3) {
				System.err.println(Constants.OPTION_ERROR_MESSAGE);
			}
			
		}while(option<1 || option>3);
		
		return option;
	}
	
	//Asks for a price:
	public float getPrice() {
		System.out.println(Constants.PRICE_REQUEST);
		return getNumberFloat();
	}
	
	//Asks for a type:
	public char getType() {
		System.out.println(Constants.TYPE_REQUEST);
		return getText().toUpperCase().charAt(0);
	}
	
	//Prints the table header. 
	public void printHeaderTable(String tableTitle) {

		System.out.println("-------------------------------------" + tableTitle + "-------------------------------------");
		System.out.printf("%-15s", Constants.TABLE_HEADER[0]);
		System.out.printf("%-15s", Constants.TABLE_HEADER[1]);
		System.out.printf("%-20s", Constants.TABLE_HEADER[2]);
		System.out.printf("%-30s", Constants.TABLE_HEADER[3]);
		System.out.printf("%7s", Constants.TABLE_HEADER[4]);
		System.out.println();
		
	}
	
	//Prints the table body.
	public void printProductTable(Product p) {
	
		System.out.printf("%-15d", p.getCode());
		System.out.printf("%-15s", p.getType());
		System.out.printf("%-20s", p.getName());
		System.out.printf("%-30s", p.getDescription());
		System.out.printf("%7.2f", p.getPrice());
		System.out.println();
		
	}
	
	//Prints the table footer:
	public void printFootTable() {
		System.out.println("-------------------------------------------------------------------------------------------");
		System.out.println();
	}
	
	
	
}
