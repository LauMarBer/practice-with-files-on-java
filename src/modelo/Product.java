package modelo;

public class Product {
	private int code;
	private char type;
	private String name, description;
	private float price;
	
	public Product() {
		this.code = -1;
	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public char getType() {
		return type;
	}
	public void setType(char type) {
		String typee = String.valueOf(type);
		this.type = typee.toUpperCase().charAt(0);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
	//Overriding this method, so the method ".contains()" works. 
	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}if (obj == null) {
			return false;
		}if (getClass() != obj.getClass()) {
			return false;
		}
		Product other = (Product) obj;
		if((code != -1 && other.getCode() !=-1) && (name!=null && other.getName()!=null)) {
			if(code == other.getCode() && name.equals(other.getName())) {
				return true;
			}else {
				return false;
			}
		}else {
			return false;
		}
	}
	
	
	
	
	
	
}
